# kubernetes

This repository contains some of the core services deployed on kubernetes.

## Prerequisites

1. kubernetes cluster.

## To start using the deployments

1. access you cluster and copy the configuration file generated in the `./kube` directory inside your cluster.
2. create a file called `config` inside a directory on your local machine called `./kube` and paste the configuration.
3. clone the repository and enjoy deploying.

### Deploy on kubernetes

To deploy the whole application in the kubernetes cluster inside a specific namespace :

1. Refer to the application directory.
2. 
    ``` sh 
    kubectl apply -f . -n namespace    
    ```
